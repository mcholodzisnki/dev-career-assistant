package eu.mzsolutions.osr.gihub.crawler.http;

import org.apache.commons.io.IOUtils;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by zason on 10/31/2016.
 */
public class GithubHttpClient {


    public GithubResponse getGithubHttp(String stringUrl) {
        URL url = null;
        BufferedReader reader = null;
        StringBuilder stringBuilder;
        try {
            url = new URL(stringUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            BASE64Encoder enc = new sun.misc.BASE64Encoder();
            String userpassword = "Borsuk23:friendsfor3ever";
            String encodedAuthorization = enc.encode(userpassword.getBytes());
            connection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);

            connection.connect();

            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            stringBuilder = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            GithubResponse response = new GithubResponse();
            response.setContent(stringBuilder.toString());
            if (connection.getHeaderField("Link") != null) {
                response.setNextPageAndLastPageFromLink(connection.getHeaderField("Link"));
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new GithubResponse();
    }


    public String testUrlCall() {
        URL url = null;
        BufferedReader reader = null;
        StringBuilder stringBuilder;
        try {
            url = new URL("https://api.github.com/organizations");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            InputStream stream = connection.getInputStream();
            System.out.println(connection.getResponseCode());


            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            return stringBuilder.toString();

//            return parseInputStreamIntoString(stream);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    private String parseInputStreamIntoString(InputStream stream) {
        String encoding = "UTF-8";
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(stream, writer, encoding);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        return writer.toString();
    }
}
