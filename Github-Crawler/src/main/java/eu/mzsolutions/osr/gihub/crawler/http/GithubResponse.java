package eu.mzsolutions.osr.gihub.crawler.http;

/**
 * Created by zason on 10/31/2016.
 */
public class GithubResponse {
    private String nextPage;
    private String lastPage;
    private String content;

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPageAndLastPageFromLink(String link) {
//        <https://api.github.com/organizations?since=3043>; rel="next", <https://api.github.com/organizations{?since}>; rel="first"
        String[] links = link.split(",");
        nextPage = null;
        lastPage = null;
        for (String singleLink : links) {
            if (singleLink.contains("next")) {
                nextPage = singleLink.split("<")[1].split(">")[0];
            } else if (singleLink.contains("last")) {
                lastPage = singleLink.split("<")[1].split(">")[0];
            }
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLastPage() {
        return lastPage;
    }

}
