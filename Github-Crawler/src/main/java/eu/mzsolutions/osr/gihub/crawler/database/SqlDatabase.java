/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.mzsolutions.osr.gihub.crawler.database;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is single thread version!
 *
 * @author mateusz
 */
public class SqlDatabase {

    private static SqlDatabase instance = null;

    private Connection connection = null;
    private String databaseUrl = null;
    private String userName = null;
    private String password = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    public static SqlDatabase getInstance() {
        if (instance == null) {
            instance = new SqlDatabase();
        }
        return instance;
    }

    public synchronized boolean connect() {
        if (this.connection != null) {
            return true;
        }
        Logger.getLogger(SqlDatabase.class.getName()).log(Level.INFO, "Loading database configuration");
        loadConfiguration();

        Logger.getLogger(SqlDatabase.class.getName()).log(Level.INFO, "Initializing driver.");
        try {
            initialize();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SqlDatabase.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        Logger.getLogger(SqlDatabase.class.getName()).log(Level.INFO, databaseUrl);

        Logger.getLogger(SqlDatabase.class.getName()).log(Level.INFO, "Connecting to database.");
        try {
            connection = DriverManager.getConnection(databaseUrl, userName, password);
        } catch (SQLException ex) {
            Logger.getLogger(SqlDatabase.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public synchronized boolean disconnect() {
        try {
            connection.close();
            connection = null;
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(SqlDatabase.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    private SqlDatabase() {

    }

    public synchronized void loadConfiguration() {
        databaseUrl = "jdbc:mysql://localhost/osr?serverTimezone=UTC";
        userName = "osr";
        password = "osr";

    }

    private synchronized void initialize() throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
    }

    public ResultSet executeQuery(String query) throws SQLException {
        statement = null;
        statement = connection.createStatement();
        return statement.executeQuery(query);
    }

    public synchronized ResultSet executeQuery(PreparedStatement preparedStatement) throws SQLException {
        return preparedStatement.executeQuery();
    }

    public synchronized int executeUpdate(PreparedStatement preparedStatement) throws SQLException {
        return preparedStatement.executeUpdate();
    }

    public synchronized PreparedStatement getPreparedStatement(String query) throws SQLException {
        if (this.preparedStatement != null) {
            this.preparedStatement.close();
            this.preparedStatement = null;
        }
        this.preparedStatement = this.connection.prepareStatement(query);
        return this.preparedStatement;
    }

    public synchronized void executeBatch(PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.executeBatch();
    }

    public synchronized Connection getConnection() throws SQLException {
        return DriverManager.getConnection(databaseUrl, userName, password);

    }

    public synchronized PreparedStatement getPreparedStatement(Connection conn, String query) throws SQLException {
        return conn.prepareStatement(query);
    }

}
