package eu.mzsolutions.osr.gihub.crawler.database;

import com.mongodb.DB;
import com.mongodb.MongoClient;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zason on 10/31/2016.
 */
public class MongoDatabase {

    MongoClient mongoClient;

    public void initialize() throws UnknownHostException {
        mongoClient = new MongoClient("localhost", 27017);
    }


    public DB getDatabaseConnection() {
        return mongoClient.getDB("mydb");
    }
}
