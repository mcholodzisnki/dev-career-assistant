package eu.mzsolutions.osr.gihub.crawler.core;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.*;
import com.mongodb.util.JSON;
import eu.mzsolutions.osr.gihub.crawler.database.MongoDatabase;
import eu.mzsolutions.osr.gihub.crawler.database.SqlDatabase;
import eu.mzsolutions.osr.gihub.crawler.http.GithubHttpClient;
import eu.mzsolutions.osr.gihub.crawler.http.GithubResponse;

import java.net.UnknownHostException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * Created by zason on 10/31/2016.
 */
public class Application {

    public static void main(String[] args) {

        MongoDatabase database = new MongoDatabase();
        try {
            database.initialize();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        DB connection = database.getDatabaseConnection();

//        getUsersForRepose(connection, "mozilla");
//        getUsersForRepose(connection, "wikimedia");

        pumpFromMongoToSQL3(connection, "mozilla");
        pumpFromMongoToSQL3(connection, "wikimedia");
        pumpFromMongoToSQL3(connection, "eclipse");
        pumpFromMongoToSQL3(connection, "WordPress");
    }


    private static void getRepos(DB connection, String org) {
        GithubHttpClient httpClient = new GithubHttpClient();
        String nextUrl = "https://api.github.com/orgs/" + org + "/repos?page=1";
        do {
            GithubResponse response = httpClient.getGithubHttp(nextUrl);

            nextUrl = response.getNextPage();
            JsonParser parser = new JsonParser();
            JsonArray arr = (JsonArray) parser.parse(response.getContent());

            DBCollection collection = connection.getCollection(org + "-repos");

            Iterator<JsonElement> itr = arr.iterator();
            while (itr.hasNext()) {
                JsonElement val = itr.next();
                DBObject dbObject = (DBObject) JSON.parse(val.toString());
                collection.insert(dbObject);
            }
        }
        while (nextUrl != null);

    }

    private static void getMembers(DB connection, String org) {
        GithubHttpClient httpClient = new GithubHttpClient();
        String nextUrl = "https://api.github.com/orgs/" + org + "/members?page=1";
        do {
            GithubResponse response = httpClient.getGithubHttp(nextUrl);

            nextUrl = response.getNextPage();
            JsonParser parser = new JsonParser();
            JsonArray arr = (JsonArray) parser.parse(response.getContent());

            DBCollection collection = connection.getCollection(org + "-members");

            Iterator<JsonElement> itr = arr.iterator();
            while (itr.hasNext()) {
                JsonElement val = itr.next();
                DBObject dbObject = (DBObject) JSON.parse(val.toString());
                collection.insert(dbObject);
            }
        }
        while (nextUrl != null);
    }


    private static void getUsers(DB connection, String org) {
        GithubHttpClient httpClient = new GithubHttpClient();
        DBCollection collection = connection.getCollection(org + "-members");
        DBCursor dbObjects = collection.find();
        while (dbObjects.hasNext()) {
            DBObject next = dbObjects.next();
            String url = (String) next.get("url");
            GithubResponse response = httpClient.getGithubHttp(url);

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = (JsonObject) parser.parse(response.getContent());
            DBCollection userCol = connection.getCollection("users");
            DBObject dbObject = (DBObject) JSON.parse(jsonObject.toString());
            try {
                userCol.insert(dbObject);
            } catch (DuplicateKeyException ex) {
                System.out.println("duplicated: " + jsonObject.toString());
            }
        }

    }


    private static void getMembersForRepos(DB connection, String org) {
        GithubHttpClient httpClient = new GithubHttpClient();
        DBCollection collection = connection.getCollection(org + "-repos");
        DBCollection reposMembers = connection.getCollection(org + "-repos-members");
        DBCursor dbObjects = collection.find();
        while (dbObjects.hasNext()) {
            DBObject next = dbObjects.next();
            String nextUrl = (String) next.get("contributors_url");

            do {
                GithubResponse response = httpClient.getGithubHttp(nextUrl);

                nextUrl = response.getNextPage();
                JsonParser parser = new JsonParser();
                JsonArray arr = (JsonArray) parser.parse(response.getContent());

                Iterator<JsonElement> itr = arr.iterator();
                while (itr.hasNext()) {
                    JsonElement val = itr.next();
                    val.getAsJsonObject().addProperty("projectName", (String) next.get("name"));
                    DBObject dbObject = (DBObject) JSON.parse(val.toString());
                    reposMembers.insert(dbObject);
                }
            } while (nextUrl != null);

        }

    }

    private static void getUsersForRepose(DB connection, String org) {
        getUsers(connection, org + "-repos");
    }


    private static void pumpFromMongoToSQL(DB connection, String org) {
        SqlDatabase sqlDatabase = SqlDatabase.getInstance();
        sqlDatabase.connect();
        try {
            Connection sqlDatabaseConnection = sqlDatabase.getConnection();

            DBCollection collection = connection.getCollection(org + "-repos");
            DBCursor dbObjects = collection.find();

            int orgId = getOrgId(sqlDatabase, org);

            String insertQuery = "INSERT INTO git_projects (name, full_name, language, created_at, updated_at, id_org) VALUES (?,?,?,?,?,?)";
            PreparedStatement sqlInsertStatement = sqlDatabase.getPreparedStatement(sqlDatabaseConnection, insertQuery);

            while (dbObjects.hasNext()) {

                DBObject next = dbObjects.next();

                String name = (String) next.get("name");
                String fullName = (String) next.get("full_name");
                String language = (String) next.get("language");
                String createdAt = (String) next.get("created_at");
                String updatedAt = (String) next.get("updated_at");

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                sqlInsertStatement.setString(1, name);
                sqlInsertStatement.setString(2, fullName);
                sqlInsertStatement.setString(3, language);
                sqlInsertStatement.setTimestamp(4, new java.sql.Timestamp((simpleDateFormat.parse(createdAt)).getTime()));
                sqlInsertStatement.setTimestamp(5, new java.sql.Timestamp((simpleDateFormat.parse(updatedAt)).getTime()));
                sqlInsertStatement.setInt(6, orgId);
                try {
                    sqlDatabase.executeUpdate(sqlInsertStatement);
                } catch (SQLIntegrityConstraintViolationException ex) {

                }
//                sqlInsertStatement.addBatch();
            }
            sqlDatabaseConnection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    private static void pumpFromMongoToSQL3(DB connection, String org) {
        SqlDatabase sqlDatabase = SqlDatabase.getInstance();
        sqlDatabase.connect();
        try {
            Connection sqlDatabaseConnection = sqlDatabase.getConnection();

            DBCollection collection = connection.getCollection(org + "-repos-members");
            DBCursor dbObjects = collection.find();

            int orgId = getOrgId(sqlDatabase, org);

            String insertQuery = "INSERT INTO git_members_orgs (id_git_users, id_git_orgs, id_git_proj) VALUES (?,?,?)";
            PreparedStatement sqlInsertStatement = sqlDatabase.getPreparedStatement(sqlDatabaseConnection, insertQuery);

            while (dbObjects.hasNext()) {

                DBObject next = dbObjects.next();

                String projectName = (String) next.get("projectName");
                String login = (String) next.get("login");

                int projId = getProjId(sqlDatabase, sqlDatabaseConnection, projectName);
                int userID = getUserId(sqlDatabase, sqlDatabaseConnection, login);

                sqlInsertStatement.setInt(1, userID);
                sqlInsertStatement.setInt(2, orgId);
                sqlInsertStatement.setInt(3, projId);
                try {
                    sqlDatabase.executeUpdate(sqlInsertStatement);
                } catch (SQLIntegrityConstraintViolationException ex) {

                }
            }
            sqlDatabaseConnection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static int getUserId(SqlDatabase sqlDatabase, Connection connection, String login) {
        String getOrgIdQuery = "SELECT id FROM git_users WHERE login =?";
        try {
            PreparedStatement contribSelect = sqlDatabase.getPreparedStatement(connection, getOrgIdQuery);
            contribSelect.setString(1, login);
            ResultSet resultSet = sqlDatabase.executeQuery(contribSelect);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static int getProjId(SqlDatabase sqlDatabase, Connection connection, String projectName) {
        String getOrgIdQuery = "SELECT id FROM git_projects WHERE name =?";
        try {
            PreparedStatement contribSelect = sqlDatabase.getPreparedStatement(connection, getOrgIdQuery);
            contribSelect.setString(1, projectName);
            ResultSet resultSet = sqlDatabase.executeQuery(contribSelect);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static void pumpFromMongoToSQL2(DB connection) {
        SqlDatabase sqlDatabase = SqlDatabase.getInstance();
        sqlDatabase.connect();
        try {
            Connection sqlDatabaseConnection = sqlDatabase.getConnection();

            DBCollection collection = connection.getCollection("users");
            DBCursor dbObjects = collection.find();


            String insertQuery = "INSERT INTO git_users (login, git_id, name, company, location, email) VALUES (?,?,?,?,?,?)";
            PreparedStatement sqlInsertStatement = sqlDatabase.getPreparedStatement(sqlDatabaseConnection, insertQuery);

            while (dbObjects.hasNext()) {

                DBObject next = dbObjects.next();

                String login = (String) next.get("login");
                int id = (int) next.get("id");
                String name = (String) next.get("name");
                String company = (String) next.get("company");
                String location = (String) next.get("location");
                String email = (String) next.get("email");


                // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                sqlInsertStatement.setString(1, login);
                sqlInsertStatement.setInt(2, id);
                sqlInsertStatement.setString(3, name);
                sqlInsertStatement.setString(4, company);
                sqlInsertStatement.setString(5, location);
                sqlInsertStatement.setString(6, email);

                //   sqlInsertStatement.setTimestamp(4, new java.sql.Timestamp((simpleDateFormat.parse(createdAt)).getTime()));
                //   sqlInsertStatement.setTimestamp(5, new java.sql.Timestamp((simpleDateFormat.parse(updatedAt)).getTime()));
                //   sqlInsertStatement.setInt(6, orgId);
                try {
                    sqlDatabase.executeUpdate(sqlInsertStatement);
                } catch (SQLIntegrityConstraintViolationException ex) {

                }
//                sqlInsertStatement.addBatch();
            }
            sqlDatabaseConnection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    private static int getOrgId(SqlDatabase sqlDatabase, String orgName) {
        String getOrgIdQuery = "SELECT id FROM orgs WHERE urlName =?";
        Connection connection = null;
        try {
            connection = sqlDatabase.getConnection();
            PreparedStatement contribSelect = sqlDatabase.getPreparedStatement(connection, getOrgIdQuery);
            contribSelect.setString(1, orgName.toLowerCase());
            ResultSet resultSet = sqlDatabase.executeQuery(contribSelect);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            connection.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }

}
