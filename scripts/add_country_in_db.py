import mysql.connector
import requests
import json
import time
from mysql.connector import Error

YOUR_API_KEY = 'TWOJ KLUCZ'

try:
	# Konfiguracja polaczenia z baza
	conn = mysql.connector.connect(host='localhost', database='tass_db', user='root', password='root')
	if conn.is_connected():
		print('Connected to MySQL database') 
except Error as e:
	print(e)

cursor = conn.cursor()
cursor.execute("SELECT openhub_id, git_location from contributors where openhub_id >= 27500")
rows = cursor.fetchall()

for row in rows:
	if row[1] is not None:
		print("Wysylam request do serwera")
		r = requests.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + row[1] + '&key=' + YOUR_API_KEY)
		print("Pobrano dane z serwera")
		data_from_server = r.json()
		if len(data_from_server['results']) > 0:
			address_components = data_from_server['results'][0]['address_components'];
			for component in address_components:
				if 'country' in component['types']:
					country = component['long_name']
			if country is not None:
				print("Insert: " + country) 
				query = "UPDATE contributors SET country = \'" + country + "\' where openhub_id = " + str(row[0])
				cursor.execute(query)
				conn.commit()
		time.sleep(2)

cursor.close()
conn.close()