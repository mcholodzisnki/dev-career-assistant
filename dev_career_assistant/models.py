# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class CodeChanges(models.Model):
	id = models.IntegerField(primary_key=True)
	language = models.IntegerField()
	code_added = models.IntegerField()
	code_removed = models.IntegerField()
	comments_added = models.IntegerField()
	comments_removed = models.IntegerField()
	blanks_added = models.IntegerField()
	blanks_removed = models.IntegerField()
	commit_id = models.IntegerField()

	class Meta:
		managed = False
		db_table = 'code_changes'
		unique_together = (('commit_id', 'language'),)


class Commits(models.Model):
	id = models.IntegerField(primary_key=True)
	contributor_id = models.IntegerField(blank=True, null=True)
	project_id = models.IntegerField(blank=True, null=True)
	comment = models.TextField(blank=True, null=True)
	commit_uuid = models.CharField(max_length=64, blank=True, null=True)
	files_modified = models.IntegerField(blank=True, null=True)
	lines_added = models.IntegerField(blank=True, null=True)
	lines_removed = models.IntegerField(blank=True, null=True)
	year = models.IntegerField(blank=True, null=True)
	month = models.IntegerField(blank=True, null=True)
	day = models.IntegerField(blank=True, null=True)
	hour = models.IntegerField(blank=True, null=True)
	minutes = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'commits'


class Contributors(models.Model):
	openhub_id = models.IntegerField(blank=True, null=True)
	openhub_name = models.CharField(max_length=256, blank=True, null=True)
	openhub_project_id = models.IntegerField(blank=True, null=True)
	openhub_login = models.CharField(max_length=256, blank=True, null=True)
	git_id = models.IntegerField(blank=True, null=True)
	git_login = models.CharField(max_length=256, blank=True, null=True)
	git_name = models.CharField(max_length=256, blank=True, null=True)
	git_company = models.CharField(max_length=256, blank=True, null=True)
	git_location = models.CharField(max_length=256, blank=True, null=True)
	git_email = models.CharField(max_length=256, blank=True, null=True)
	country = models.CharField(max_length=64, blank=True, null=True)
	country_id = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'contributors'

class Countries(models.Model):
	id = models.IntegerField(primary_key=True)
	country = models.CharField(unique=True, max_length=255, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'countries'

class GitMembersOrgs(models.Model):
	id_git_users = models.IntegerField()
	id_git_orgs = models.IntegerField()
	id_git_proj = models.IntegerField()

	class Meta:
		managed = False
		db_table = 'git_members_orgs'
		unique_together = (('id_git_users', 'id_git_proj'),)


class GitProjects(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.CharField(unique=True, max_length=255)
	full_name = models.CharField(max_length=256)
	language = models.CharField(max_length=45, blank=True, null=True)
	created_at = models.DateTimeField()
	updated_at = models.DateTimeField()
	id_org = models.IntegerField()

	class Meta:
		managed = False
		db_table = 'git_projects'


class GitUsers(models.Model):
	id = models.IntegerField(primary_key=True)
	login = models.CharField(unique=True, max_length=45)
	git_id = models.IntegerField(unique=True)
	name = models.CharField(max_length=45, blank=True, null=True)
	company = models.CharField(max_length=256, blank=True, null=True)
	location = models.CharField(max_length=256, blank=True, null=True)
	email = models.CharField(max_length=256, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'git_users'

class LanguageCountries(models.Model):
	id = models.IntegerField(primary_key=True)
	language_id = models.IntegerField()
	name = models.CharField(max_length=255, blank=True, null=True)
	country = models.CharField(max_length=255, blank=True, null=True)
	lines_added = models.IntegerField(db_column='lines added', blank=True, null=True)  # Field renamed to remove unsuitable characters.
	lines_removed = models.IntegerField(db_column='lines removed', blank=True, null=True)  # Field renamed to remove unsuitable characters.
	commits_count = models.IntegerField(db_column='commits count', blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'language_countries'

class Languages(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=50)
	name_pretty = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'languages'

class Nodes(models.Model):
	id = models.IntegerField(primary_key=True)
	country_id = models.IntegerField(blank=True, null=True)
	country = models.CharField(max_length=256, blank=True, null=True)
	lang_id = models.IntegerField(blank=True, null=True)
	lang_name = models.CharField(max_length=256, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'nodes'

class Language_Country_Shared(models.Model):
	id = models.IntegerField(primary_key=True)
	language = models.IntegerField(blank=True, null=True)
	shared_commits = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = False


class OpenhubContributors(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=256, blank=True, null=True)
	project = models.IntegerField(blank=True, null=True)
	contributor_id = models.CharField(max_length=256, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'openhub_contributors'
		unique_together = (('project', 'contributor_id'),)


class Orgs(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.TextField(blank=True, null=True)
	urlname = models.TextField(db_column='urlName', blank=True, null=True)  # Field name made lowercase.
	description = models.TextField(blank=True, null=True)
	homepage = models.TextField(blank=True, null=True)
	type = models.TextField(blank=True, null=True)
	outsidecommiters = models.IntegerField(db_column='outsideCommiters', blank=True, null=True)  # Field name made lowercase.
	outsidecommits = models.IntegerField(db_column='outsideCommits', blank=True, null=True)  # Field name made lowercase.
	affiliatedcommiters = models.IntegerField(db_column='affiliatedCommiters', blank=True, null=True)  # Field name made lowercase.
	affiliatedcommits = models.IntegerField(db_column='affiliatedCommits', blank=True, null=True)  # Field name made lowercase.
	outsideprojects = models.IntegerField(db_column='outsideProjects', blank=True, null=True)  # Field name made lowercase.
	affiliatedprojects = models.IntegerField(db_column='affiliatedProjects', blank=True, null=True)  # Field name made lowercase.
	url = models.CharField(unique=True, max_length=64)

	class Meta:
		managed = False
		db_table = 'orgs'


class Projects(models.Model):
	id = models.IntegerField(primary_key=True)
	name = models.CharField(max_length=50, blank=True, null=True)
	vanity_name = models.CharField(max_length=50, blank=True, null=True)
	description = models.TextField(blank=True, null=True)
	average_rating = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
	rating_count = models.IntegerField(blank=True, null=True)
	review_count = models.IntegerField(blank=True, null=True)
	org_id = models.IntegerField(blank=True, null=True)
	url = models.CharField(unique=True, max_length=200)

	class Meta:
		managed = False
		db_table = 'projects'
