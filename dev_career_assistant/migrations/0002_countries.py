# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-06 17:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dev_career_assistant', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Countries',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('country', models.CharField(max_length=80, unique=True)),
            ],
            options={
                'db_table': 'countries',
                'managed': False,
            },
        ),
    ]
