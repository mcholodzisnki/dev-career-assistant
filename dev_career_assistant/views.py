import networkx as nx
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt, mpld3
from django.db.models import Sum
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django_tables2 import RequestConfig

from dev_career_assistant.charts import language_country_chart_view, country_language_chart_view, \
    country_language_chart_overall_view, language_country_overall_chart_view
from dev_career_assistant.forms import LanguageForm, CountryForm
from dev_career_assistant.models import Languages, LanguageCountries, Nodes
from dev_career_assistant.tables import LanguageTableView, CountriesForLanguageTableView, \
    CountryTableView, LanguagesForCountryTableView, LanguagesForLangInCountryTableView


def language_list(request):
    data = LanguageCountries.objects.order_by('language_id').values('language_id', 'name').distinct()

    if request.method == 'POST':
        form = LanguageForm(request.POST)
        if form.is_valid():
            data = data.filter(name__contains=form.cleaned_data['language'])
    else:
        form = LanguageForm()
    table = LanguageTableView(data)
    RequestConfig(request, paginate={'per_page': 20}).configure(table)
    return render(request, 'dev_career_assistant/language_list.html', {'table': table, 'form' : form})


def language_details(request, language_id):
    data = LanguageCountries.objects.filter(language_id=language_id).order_by('-commits_count')
    table = CountriesForLanguageTableView(data)
    RequestConfig(request, paginate={'per_page': 18}).configure(table)
    chart_column = language_country_chart_view(data, 'column', 'Commits count per country:')
    chart_pie = language_country_chart_view(data[:5], 'pie', 'Countries with the greatest commits count:')
    language = Languages.objects.get(pk=language_id)

    return render(request, 'dev_career_assistant/language_detail.html', {'table' : table,
                                                                'language' : language.name,
                                                                'charts' : [chart_column, chart_pie]})

def country_list(request):
    data = LanguageCountries.objects.order_by('country').values('country').distinct()

    if request.method == 'POST':
        form = CountryForm(request.POST)
        if form.is_valid():
            data = data.filter(country__contains=form.cleaned_data['country'])
    else:
        form = CountryForm()
    table = CountryTableView(data)
    RequestConfig(request, paginate={'per_page': 20}).configure(table)
    return render(request, 'dev_career_assistant/country_list.html', {'table': table, 'form': form})


def country_details(request, country):
    data = LanguageCountries.objects.filter(country=country).order_by('-commits_count')
    table = LanguagesForCountryTableView(data)
    RequestConfig(request, paginate={'per_page': 18}).configure(table)
    chart_column = country_language_chart_view(data, 'column', 'Commits count per language:')
    chart_pie = country_language_chart_view(data[:5], 'pie', 'Languages with the greatest commits count in ' + \
                                            country +':')
    return render(request, 'dev_career_assistant/country_detail.html', {'table': table,
                                                                         'country': country,
                                                                         'charts': [chart_column, chart_pie]})

def overall_details(request):

    data = LanguageCountries.objects.values('name').annotate(commits_total=Sum('commits_count')).order_by('-commits_total')
    chart_column_languages = country_language_chart_overall_view(data, 'column', 'Commits count per language overall:')
    chart_pie_languages = country_language_chart_overall_view(data[:10], 'pie', 'Languages with the greatest commits count overall:')
    data = LanguageCountries.objects.values('country').annotate(commits_total=Sum('commits_count')).order_by(
        '-commits_total')
    chart_column_countries = language_country_overall_chart_view(data, 'column', 'Commits count per country overall:')
    chart_pie_countries = language_country_overall_chart_view(data[:10], 'pie', 'Countries with the greatest commits count overall:')
    return render(request, 'dev_career_assistant/overall_detail.html', { 'charts': [chart_column_languages,
                                                                                    chart_pie_languages,
                                                                                    chart_column_countries,
                                                                                    chart_pie_countries]})

def language_in_country_details(request, language_id, country):
    G = nx.read_edgelist('graph_data.txt', nodetype=int, data=(('amount', int),))
    nodes_id = Nodes.objects.values('id').filter(country=country)
    id_array = []
    for node_id in nodes_id:
        id_array.append(node_id.get('id'))
    node = Nodes.objects.values('id', 'lang_name').get(country=country, lang_id=language_id)
    edges = sorted(G.subgraph(id_array).edges(nbunch=[node.get('id')], data='amount'), key=lambda k: -k[2])
    edges = edges[:5]
    ids = []
    for edge in edges:
        ids.append(edge[1])
    languages = Nodes.objects.filter(country=country, id__in = ids)
    ids.append(node.get('id'))
    table_data = []
    lang = node.get('lang_name')
    labels = {node.get('id') : lang}
    i = 0
    for edge in edges:
        i += 1
        for language in languages:
            if (language.id == edge[1]):
                table_data.append({'id' : i , 'language' : language.lang_name, 'shared_commits' : edge[2]})
                labels[language.id] = language.lang_name
    table = LanguagesForLangInCountryTableView(table_data)
    SG = G.subgraph(ids)

    edge_labels = dict([((u, v,), d)
                        for u, v, d in edges])
    plt.figure(1)
    pos = nx.spring_layout(SG)
    if (len(labels) > 1):
        nx.draw_networkx(SG, arrows=True, pos=pos, with_labels=True, labels=labels, node_color='b', node_size=1200, font_size=16, font_color='r')
    if (len(edge_labels) > 1):
        nx.draw_networkx_edge_labels(SG, pos=pos, edge_labels=edge_labels, font_size=14, font_color='r')
    figure = mpld3.fig_to_html(plt.figure(1))
    plt.figure(1).clear()
    return render(request, 'dev_career_assistant/language_country_detail.html', {'edges' : edges,
                                                                                 'country': country,
                                                                                 'language' : node.get('lang_name'),
                                                                                 'table' : table,
                                                                                 'figure' : mark_safe(figure)})

def about(request):
    return render(request, 'dev_career_assistant/about.html', { })
