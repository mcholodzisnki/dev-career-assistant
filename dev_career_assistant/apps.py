import mysql.connector
from django.apps import AppConfig
from mysql.connector import Error


class DevCareerAssistantConfig(AppConfig):
    name = 'dev_career_assistant'

    def ready(self):
        try:
            conn = mysql.connector.connect(host='rds-mysql-tassproject.ctwdo9zkegnt.eu-west-1.rds.amazonaws.com', database='tass_db', user='tass_admin', password='tass_pass')
            if conn.is_connected():
                print('Connected to database')
        except Error as e:
            print(e)

        cursor = conn.cursor()
        cursor.execute("SELECT node1_id, node2_id, amount from v_langs_nodes_cnt;")
        rows = cursor.fetchall()
        print('Retrieved data to create edgelist')
        f = open('graph_data.txt', 'w')
        for row in rows:
            f.write(str(row[0]) + ' ' + str(row[1]) + ' ' + str(row[2]) + '\n')
        f.close()
        print('Edgelist created')
        cursor.close()
        conn.close()
