from chartit import Chart
from chartit import DataPool

def language_country_chart_view(source, type, title):
    language_country_data = \
        DataPool(
            series=
            [{'options': {
                'source': source},
                'terms': [
                    'country',
                    'commits_count']}
            ])

    cht = Chart(
        datasource=language_country_data,
        series_options=
        [{'options': {
            'type': type,
            'stacking': True},
            'terms': {
                'country': [
                    'commits_count']
            }}],
        chart_options=
        {'title': {
            'text': title},
            'xAxis': {
                'title': {
                    'text': 'Country'}}})

    return cht

def language_country_overall_chart_view(source, type, title):
    language_country_data = \
        DataPool(
            series=
            [{'options': {
                'source': source},
                'terms': [
                    'country',
                    'commits_total']}
            ])

    cht = Chart(
        datasource=language_country_data,
        series_options=
        [{'options': {
            'type': type,
            'stacking': True},
            'terms': {
                'country': [
                    'commits_total']
            }}],
        chart_options=
        {'title': {
            'text': title},
            'xAxis': {
                'title': {
                    'text': 'Country'}}})

    return cht

def country_language_chart_view(source, type, title):
    language_country_data = \
        DataPool(
            series=
            [{'options': {
                'source': source},
                'terms': [
                    'name',
                    'commits_count']}
            ])

    cht = Chart(
        datasource=language_country_data,
        series_options=
        [{'options': {
            'type': type,
            'stacking': True},
            'terms': {
                'name': [
                    'commits_count']
            }}],
        chart_options=
        {'title': {
            'text': title},
            'xAxis': {
                'title': {
                    'text': 'Language'}}})

    return cht

def country_language_chart_overall_view(source, type, title):
    language_country_data = \
        DataPool(
            series=
            [{'options': {
                'source': source},
                'terms': [
                    'name',
                    'commits_total']}
            ])

    cht = Chart(
        datasource=language_country_data,
        series_options=
        [{'options': {
            'type': type,
            'stacking': True},
            'terms': {
                'name': [
                    'commits_total']
            }}],
        chart_options=
        {'title': {
            'text': title},
            'xAxis': {
                'title': {
                    'text': 'Language'}}})

    return cht

def shared_commits_language_chart_view(source, type, title):
    language_country_data = \
        DataPool(
            series=
            [{'options': {
                'source': source},
                'terms': [
                    'language',
                    'shared_commits']}
            ])

    cht = Chart(
        datasource=language_country_data,
        series_options=
        [{'options': {
            'type': type,
            'stacking': True},
            'terms': {
                'language': [
                    'shared_commits']
            }}],
        chart_options=
        {'title': {
            'text': title},
            'xAxis': {
                'title': {
                    'text': 'Language'}}})

    return cht