from django.conf.urls import url
from django.views.generic import RedirectView

from . import views

urlpatterns = [
    url(r'^overall/', views.overall_details, name='overall_detail'),
    url(r'^languages/(?P<language_id>[0-9]+)/', views.language_details, name='language_detail'),
    url(r'^languages/', views.language_list, name='language'),
    url(r'^countries/(?P<country>[\w|\W]+)/(?P<language_id>[0-9]+)/', views.language_in_country_details,
        name='language_in_country_detail'),
    url(r'^countries/(?P<country>[\w|\W]+)/', views.country_details, name='country_detail'),
    url(r'^countries/', views.country_list, name='country'),
    url(r'^about', views.about, name='about'),
    url(r'^.*$', RedirectView.as_view(url='/DevCA/languages/', permanent=False), name='language')
]