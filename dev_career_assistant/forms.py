from django import forms

from dev_career_assistant.models import Languages


class LanguageForm(forms.Form):
    language = forms.CharField(label='Language', max_length=100, min_length=0, required=False, )

class CountryForm(forms.Form):
    country = forms.CharField(label='Country', max_length=100, min_length=0, required=False)