import itertools

import django_tables2 as tables


class LanguageTableView(tables.Table):
    number = tables.Column(empty_values=(), orderable=False)
    language = tables.TemplateColumn('<a href="/DevCA/languages/{{record.language_id}}/">{{record.name}}</a>')
    i = 0

    class Meta:
        attrs = {'class': 'paleblue'}

    def __init__(self, *args, **kwargs):
        super(LanguageTableView, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_number(self):
        return next(self.counter) + 1



class CountriesForLanguageTableView(tables.Table):
    id = tables.Column()
    countries = tables.TemplateColumn('<a href="/DevCA/countries/{{record.country}}/">{{record.country}}</a>')
    commits_count = tables.Column()
    i = 0
    class Meta:
        attrs = {'class': 'paleblue'}

    def __init__(self, *args, **kwargs):
        super(CountriesForLanguageTableView, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_commits_count(self, value):
        return '%s' % str(value)

    def render_id(self, value):
        self.i+=1
        return '%s' % self.i


class CountryTableView(tables.Table):
    number = tables.Column(empty_values=(), orderable=False)
    country = tables.TemplateColumn('<a href="/DevCA/countries/{{record.country}}/">{{record.country}}</a>')
    i = 0
    class Meta:
        attrs = {'class': 'paleblue'}

    def __init__(self, *args, **kwargs):
        super(CountryTableView, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_number(self):
        self.row_counter = getattr(self, 'row_counter', itertools.count())
        return next(self.row_counter) + 1

class LanguagesForCountryTableView(tables.Table):
    id = tables.Column()
    languages = tables.TemplateColumn('<a href="/DevCA/countries/{{record.country}}/{{record.language_id}}/">{{record.name}}</a>')
    commits_count = tables.Column()
    i = 0

    class Meta:
        attrs = {'class': 'paleblue'}

    def __init__(self, *args, **kwargs):
        super(LanguagesForCountryTableView, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_commits_count(self, value):
        return '%s' % str(value)

    def render_id(self, value):
        self.i += 1
        return '%s' % self.i

class LanguagesForLangInCountryTableView(tables.Table):
    id = tables.Column()
    language = tables.Column()
    shared_commits = tables.Column()
    i = 0

    class Meta:
        attrs = {'class': 'paleblue'}

    def __init__(self, *args, **kwargs):
        super(LanguagesForLangInCountryTableView, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_language(self, value):
        return '%s' % str(value)

    def render_shared_commits(self, value):
        return '%s' % str(value)

    def render_id(self, value):
        self.i += 1
        return '%s' % self.i